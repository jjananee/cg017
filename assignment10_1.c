#include <stdio.h>
int main()
{
    int m, n, x[20][20];
    printf("Enter the array dimensions (both less than 20):\nm: ");
    scanf("%d", &m);
    printf("n: ");
    scanf("%d", &n);
    printf("Enter the array elements:\n");
    for(int i=0; i<m; i++)
    {
        printf("Row %d\n", i+1);
        for(int j=0; j<n; j++) scanf("%d", &x[i][j]);
    }
    printf("\n\nThe array is:\n");
    for(int i=0; i<m; i++)
    {
        printf("\n");
        for(int j=0; j<n; j++) printf("\t%d", x[i][j]);
    }
    return 0;
}