#include <stdio.h>
int main ()
{
  int even[100], n, ele, pos, found=0;
  printf("Enter the number of elements :");
  scanf("%d", &n);
  printf("Enter the elements :\n");
  for( int i = 0; i<n; i++) scanf("%d", &even[i]);
  printf("Enter the element to be deleted :");
  scanf("%d", &ele);
  for( int i = 0; i<n; i++) 
  {
      if(even[i]==ele)
      {
        found=1;
        pos=i;
      }
  }
  if(found==1)
  {
      for( int i = pos; i<n; i++) even[i] = even[i+1];
      printf("\n");
      for( int i = 0; i<n-1; i++) printf("\n%d", even[i]);
  }
  else printf("Element not found.");
  return 0;
}