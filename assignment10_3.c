#include <stdio.h>
int main()
{
    int m, n, x1[20][20], x2[20][20];
    printf("Enter the array dimensions (both less than 20):\nm: ");
    scanf("%d", &m);
    printf("n: ");
    scanf("%d", &n);
    printf("Enter the array 1 elements:\n");
    for(int i=0; i<m; i++)
    {
        printf("Row %d   ", i+1);
        for(int j=0; j<n; j++) scanf("%d", &x1[i][j]);
    }
    printf("Enter the array 2 elements:\n");
    for(int i=0; i<m; i++)
    {
        printf("Row %d   ", i+1);
        for(int j=0; j<n; j++) scanf("%d", &x2[i][j]);
    }
    printf("\n\nThe sum of the arrays is:\n");
    for(int i=0; i<m; i++)
    {
        printf("\n");
        for(int j=0; j<n; j++) printf("\t%d", x1[i][j]+x2[i][j]);
    }
    printf("\n\nThe difference of the arrays is:\n");
    for(int i=0; i<m; i++)
    {
        printf("\n");
        for(int j=0; j<n; j++) printf("\t%d", x1[i][j]-x2[i][j]);
    }
    return 0;
}