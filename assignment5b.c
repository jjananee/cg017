#include <stdio.h>
int main ()
{
  int choice;
  printf("Pattern printer\nIf you want to print number pattern type 1\nIf you want to print alphabet pattern type 2");
  printf("\nEnter your choice : ");
  scanf("%d", &choice);
  if (choice==1)
  {
      for(int i = 1; i<4; i++)
      {
          for(int j = 1; j<=i; j++) printf("%d ",j);
          printf("\n");
      }
      
  }
  if (choice==2)
  {
      for(char i = 65; i<68; i++)
      {
          for(char j = 65; j<=i; j++) printf("%c ",j);
          printf("\n");
      }
      
  }
  return 0;
}
