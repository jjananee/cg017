#include <stdio.h>

int main()
{
    int x, p=0;
    printf("Enter a number: ");
    scanf("%d", &x);
    for(int i=2; i<=(x/2); i++)
    {
        if((x%i)==0) 
        {
            printf("\nThe number is composite");
            p=1;
            break;
        }
    }
    if(p==0) printf("\nThe number is prime");
    return 0;
}