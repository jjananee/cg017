#include <stdio.h>

int main()
{
    int x[5][3], sum;
    printf("Enter the sales:\n\n");
    printf("product    \t1\t2\t3\n");
    for(int i=0; i<5; i++)
    {
        printf("\nsalesman %d      ", i+1);
        for(int j=0; j<3; j++) scanf("%d", &x[i][j]);
    }
    for(int i=0; i<5; i++)
    {
        sum=0;
        for(int j=0; j<3; j++) 
        {
            sum+= x[i][j];
        }
        printf("\nThe total sales of salesman %d = %d", i+1, sum);
    }
    printf("\n");
    for(int i=0; i<3; i++)
    {
        sum=0;
        for(int j=0; j<5; j++) 
        {
            sum+= x[j][i];
        }
        printf("\nThe total sales of product %d = %d", i+1, sum);
    }
    return 0;
}