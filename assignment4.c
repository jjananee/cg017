#include <stdio.h>

float input ()
{ 
  float a;
  scanf("%f", &a);
  return a;
}
float area (float r)
{ 
  float ar = r*r*3.14;
  return ar;
}
void output (float t)
{ 
  printf("\nThe area of the circle is : % f", t);
}
int main()
{ 
  float r;
  printf("\nenter radius value:");
  r=input();
  float a = area(r);
  output(a);
  return 0;
}
