#include <stdio.h>

int main()
{
	char ch;
	printf("Enter a character: ");
	scanf("%c", &ch);
	int asc = ch;
	printf("The ascii value of the character is %d",asc);
	return 0;
}