#include <stdio.h>
int main()
{
    int n; 
    float sum = 0;
    printf("Enter the number of inverse squares to be added : ");
    scanf("%d", &n);
    for(int i = 1; i<=n; i++) sum = sum + (float)1/(i*i);
    printf("\n The sum of inverse squares is %f", sum);
    return 0;
}