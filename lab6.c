#include <stdio.h>
int main()
{
    int x[5][3], large;
    printf("Enter the marks:\n");
    for(int i=0; i<5; i++)
    {
        printf("\t\tSTUDENT %d\n", i+1);
        for(int j=0; j<3; j++) 
        {
            printf("subject %d: ", j+1);
            scanf("%d", &x[i][j]);
        }
    }
    for(int i=0; i<3; i++)
    {
        large = 0;
        for(int j=0; j<5; j++) if(large<x[j][i]) large=x[j][i];
        printf("\nThe highest marks for subject %d is %d.", i+1, large);
    }
    
    return 0;
}