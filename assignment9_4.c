#include <stdio.h>
#include <math.h>

int main()
{
    int x, b=0, i=0;
    printf("Enter a number: ");
    scanf("%d", &x);
    i=x;
    while(x!=0)
    {
        b=(b*10)+(x%10);
        x=x/10;
    }
    if(i==b) printf("\nThe number is palindrome");
    else printf("\nThe number is not palindrome");
    return 0;
}