#include <stdio.h>
struct student
{
    int rollnumber;
    char name[60];
    char section;
    char department[60];
    float fees;
    float results;
}s[2];
int main()
{
    int x;
    float large;
    for(int i; i<2; i++)
    {
        printf("Enter student %d details: \n", i+1);
        printf("department: ");
        gets(s[i].department);
        printf("name: ");
        gets(s[i].name);
        printf("section: ");
        scanf("%c", &s[i].section);
        printf("roll number: ");
        scanf("%d", &s[i].rollnumber);
        printf("fees: ");
        scanf("%f", &s[i].fees);
        printf("results: ");
        scanf("%f", &s[i].results);
        if(large<s[i].results)
        {
            x=i;
            large=s[i].results;
        }
    }
    printf("\n\n\t\tThe student with higher scores is:");
    printf("\nName : %s\nDepartment : %s\nSection : %c\nRoll no. : %d\nFees : %f\nResults: %f",
           s[x].name,s[x].department,s[x].section,s[x].rollnumber,s[x].fees,s[x].results);
    return 0;
}