#include <stdio.h>

int main()
{
    char ch;
    int up=0, low=0, num=0;
    printf("Enter characters (* to stop) :\n");
    do
    {
        scanf("%c", &ch);
        if(ch>='a'&&ch<='z') low++;
        else if(ch>='A'&&ch<='Z') up++;
        else if(ch>='0'&&ch<='9') num++;
    }
    while(ch!='*');
    printf("\nThe number of lowercase letters = %d\nThe number of uppercase letters = %d\nThe number of numbers = %d", low, up, num);
    return 0;
}