#include <stdio.h>
#include <math.h>

int main()
{ 
  int a, b, c;
  float r1, r2;
  printf("Enter the values of the co-efficients in order");
  scanf("%d %d %d", &a, &b, &c);
  float d= ((b*b)-(4*a*c));
  if (d==0)
  {
    r1= (float)(-b/(2*a));
    printf("\nthe roots are real and equal: %f", r1);
  }
  else 
  {
    if (d>0)
    {
      r1=(-b+sqrt(d)/(2*a));
      r2=(-b-sqrt(d)/(2*a));
      printf("\nDistinct and real roots : %f,%f ",r1, r2);
    }
    else
    {
      printf("\nThe roots are imaginary");
      float real=-((float)(b)/(2*a));
      d=d*-1;
      float img= sqrt(d)/(2*a);
      printf("\n%f+i%f",real,img);
      printf("\n%f-i%f",real,img);
    }
  }
  return 0;
}