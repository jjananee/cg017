#include <stdio.h>
#include <stdlib.h>
int main()
{
    char c;
    FILE *fp = fopen("INPUT.txt", "w");
    if(fp==NULL)
    {
        printf("Error");
        exit(1);
    }
    printf("Enter characters:\n");
    while((c=getchar())!=EOF)
    {
       fputc(c, fp);
    }
    fclose(fp);
    c='a';
    fp = fopen("INPUT.txt", "r");
    if(fp==NULL)
    {
        printf("Error");
        exit(1);
    }
    printf("\n");
    while((c=fgetc(fp))!=EOF)
    {
       putchar(c);
    }
    fclose(fp);
    return 0;
}
