#include <stdio.h>
int main()
{
    int n, sum = 0;
    printf("Enter the number of squares to be added : ");
    scanf("%d", &n);
    for(int i = 1; i<=n; i++) sum = sum + (i*i);
    printf("\n The sum of %d squares is %d ", n, sum);
    return 0;
}