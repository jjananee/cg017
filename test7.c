#include <stdio.h>
int main()
{
    int m, n, x1[20][20], x2[20][20];
    printf("Enter the array dimensions (both less than 20):\nm: ");
    scanf("%d", &m);
    printf("n: ");
    scanf("%d", &n);
    printf("Enter the array elements:\n");
    for(int i=0; i<m; i++)
    {
        printf("Row %d   ", i+1);
        for(int j=0; j<n; j++) scanf("%d", &x1[i][j]);
    }
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<m; j++) x2[i][j]=x1[j][i];
    }
    printf("\n\nThe transpose of the array is:\n");
    for(int i=0; i<n; i++)
    {
        printf("\n");
        for(int j=0; j<m; j++) printf("\t%d", x2[i][j]);
    }
    return 0;
}