#include <stdio.h>
void swap (int* , int*);

int main()
{
    int a, b;
    printf("Enter two numbers:\n");
    scanf("%d %d", &a, &b);
    printf("\n before swapping: %d   %d", a, b);
    swap(&a, &b);
    printf("\n after swapping: %d   %d", a, b);
    return 0;
}

void swap (int* x, int* y)
{
    int z;
    z = *x;
    *x = *y;
    *y = z;
}