#include <stdio.h>
#include <math.h>

int main()
{
    int x, b=0, i=0;
    printf("Enter a number: ");
    scanf("%d", &x);
    while(x!=0)
    {
        b=b+(x%2)*pow(10,i);
        i++;
        x=x/2;
    }
    printf("\nThe number is %d in binary", b);
    return 0;
}