#include <stdio.h>
#include <math.h>
int main()
{
    float a, b, c;
    int x;
    printf("Enter the co-efficients of the quadratic equation in order: ");
    scanf("%f %f %f", &a, &b, &c);
    float d = (b*b)-(4*a*c);
    if (d==0) x = 0;
    else if (d>0) x = 1;
    else x = -1;
    switch(x)
    {
        case -1 : printf("\nThe roots are imaginary");
                  float real, imag;
                  real=(-b)/(2*a);
                  imag=sqrt(-d)/(2*a);
                  printf("\n%f+i%f\n%f-i%f", real, imag, real, imag);
                  break;
        case 0 :  printf("\nThe roots are real and equal");
                  printf("\n%f", ((-b)/(2*a)));
                  break;
        case 1 :  printf("\nThe roots are real");
                  float r1, r2;
                  r1=((-b)+sqrt(d))/(2*a);
                  r2=((-b)-sqrt(d))/(2*a);
                  printf("\n%f\n%f", r1, r2);
                  break;
        default : printf("\nHow??");
    }
    return 0;
}