#include <stdio.h>
int main ()
{
  int odd[100], n, ele, pos;
  printf("Enter the number of elements :");
  scanf("%d", &n);
  printf("Enter the elements :\n");
  for( int i = 0; i<n; i++) scanf("%d", &odd[i]);
  printf("Enter the element to be added :");
  scanf("%d", &ele);
  printf("Enter the element's position :");
  scanf("%d", &pos);
  for( int i = n; i>pos; i--) odd[i] = odd[i-1];
  odd[pos]=ele;
  printf("\n");
  for( int i = 0; i<n+1; i++) printf("\n%d", odd[i]);
  return 0;
}