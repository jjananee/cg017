#include <stdio.h>

int main()
{ int a, sum=0;
  printf("Enter a number:");
  scanf("%d", &a);
  while(a)
  { sum = sum + (a%10);
	a = a/10;
  }
  printf("\nThe sum of the digits is %d", sum);
  return 0;
}  
