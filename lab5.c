#include <stdio.h>
int main()
{
    int x[100], n, temp, pos1, pos2;
    printf("Enter the number of elements: ");
    scanf("%d", &n);
    printf("Enter the elements:\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d", &x[i]);
    }
    int large=x[0], small=x[0];
    for(int i=0;i<n;i++)
    {
        if(x[i]>large) 
        {
            large = x[i];
            pos1=i;
        }
        if(x[i]<small)
        {
            small = x[i];
            pos2=i;
        }
    }
    temp=x[pos1];
    x[pos1]=x[pos2];
    x[pos2]=temp;
    printf("\nThe numbers have been exchanged");
    for(int i=0;i<n;i++)
    {
        printf("\n%d", x[i]);
    }

    return 0;
}