#include <stdio.h>
int main()
{
    int x[100], n, sum=0, avg;
    printf("Enter the number of elements: ");
    scanf("%d", &n);
    printf("Enter the elements:\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d", &x[i]);
    }
    for(int i=0;i<n;i++)
    {
        sum+=x[i];
    }
    avg=sum/n;
    printf("\nThe average of the numbers is %d", avg);

    return 0;
}