#include <stdio.h>

int input ()
{ int a;
  
  scanf("%d", &a);
  return a;
 }
int time (int h, int m)
{ int tot = h*60 + m;
  return tot;
}
void output (int t)
{ printf("\nThe total time in minutes is : % d", t);
}
int main()
{ int a, b;
  printf("\nenter hour value:");
  a=input();
  printf("\nenter minute value:");
  b=input();
  int t =time(a,b);
  output(t);
  return 0;
 }