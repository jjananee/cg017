#include <stdio.h>

int main()
{   
    int a, b;
    printf("Enter two values to add subtract multiply and divide:\n");
    scanf("%d %d",&a ,&b);
    printf("\n%d + %d = %d", a, b, (a+b));
    printf("\n%d - %d = %d", a, b, (a-b));
    printf("\n%d * %d = %d", a, b, (a*b));
    printf("\n%d / %d = %f", a, b, (float)(a/b));
    printf("\n%d %% %d = %d", a, b, (a%b));
    return 0;
}