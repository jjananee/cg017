#include <stdio.h>
int* add (int* , int*);
int z;
int* sub (int* , int*);
int* mul (int* , int*);
int* divi (int* , int*);
int* rem (int* , int*);

int main()
{
    int a, b, *c, *d, *e;
    c=&a;
    d=&b;
    printf("Enter two numbers:\n");
    scanf("%d %d", c, d);
    e=add(c, d);
    printf("\n %d + %d = %d", *c, *d, *e);
    e=sub(c, d);
    printf("\n %d - %d = %d", *c, *d, *e);
    e=mul(c, d);
    printf("\n %d * %d = %d", *c, *d, *e);
    e=divi(c, d);
    printf("\n %d / %d = %d", *c, *d, *e);
    e=rem(c, d);
    printf("\n %d %% %d = %d", *c, *d, *e);
    return 0;
}

int* add (int* x, int* y)
{
    z = *x + *y;
    return &z;
}
int* sub (int* x, int* y)
{
    z = *x - *y;
    return &z;
}
int* mul (int* x, int* y)
{
    z = *x * *y;
    return &z;
}
int* divi (int* x, int* y)
{
    z = *x / *y;
    return &z;
}
int* rem (int* x, int* y)
{
    z = *x % *y;
    return &z;
}